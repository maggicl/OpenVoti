<!--- vim: set et ts=2 sw=2 tw=80 : -->

# OpenVoti
Ionic 3 app to see marks from the italian online e-school register Argo 
Scuolanext. Unfortunately, this project contains italian harcoded strings.

## Abandoned
This was a little experiment coded in the 2017-2018 christmas break. This code 
is unmaintained.
