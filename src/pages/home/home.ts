/**
 * OpenVoti - Unofficial client for the Argo Scuolanext eDiary
 * Copyright (C) 2017 Claudio Maggioni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

import { Component } from '@angular/core';
import { NavController, AlertController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfigPage } from '../config/config';

export interface ArgoUser {
    username: string;
    password: string;
    schoolCode: string;
}

export interface ArgoMark {
    value: number;
    type: string;
    date: Date;
}

export interface ArgoSubject {
    name: string;
    marks: ArgoMark[];
}

export interface ArgoMarks {
    success: boolean;
    message?: string;
    stackTrace?: string;
    subjects: ArgoSubject[];
}

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    data: any = { subjects: [] }

    constructor(public navCtrl: NavController, protected alertCtrl: AlertController,
        protected storage: Storage, protected toastCtrl: ToastController) {
        this.getMarks();
    }

    warnNoData(): void {
        this.toastCtrl.create({
            message: "Inserire i propri dati di accesso.",
            duration: 5000
        }).present();
        this.navCtrl.setRoot(ConfigPage);
    }

    doRefresh(refresher): void {
        this.getMarks().then(_ => refresher.complete());
    }

    private getMarks(): Promise<void> {
        return new Promise<void>((resolve, _) => {
            this.storage.get('user').then((user: ArgoUser) => {
                console.debug("ArgoUser is: " + JSON.stringify(user));
                if (user == null) {
                    console.debug("User in storage is null");
                    this.warnNoData();
                    resolve();
                }

                window["cordova"].plugins.ArgoAPIPlugin.getMarks({
                    username: user.username,
                    password: user.password,
                    schoolCode: user.schoolCode,
                }).then((marks: ArgoMarks) => {
                    console.debug("Got mark data: " + JSON.stringify(marks));
                    this.data = marks;
                    resolve();
                }).catch((err: ArgoMarks) => {
                    console.debug("Error in ArgoAPIPlugin call: " + JSON.stringify(err))
                    this.alertCtrl.create({
                        title: "Lettura voti fallita:",
                        subTitle: err.message,
                        buttons: ['Ok']
                    });
                    resolve();
                });
            }).catch(e => {
                console.debug("Error in Storage fetch: " + JSON.stringify(e));
                this.warnNoData();
                resolve();
            });
        });
    }
}
