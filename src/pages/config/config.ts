/**
 * OpenVoti - Unofficial client for the Argo Scuolanext eDiary
 * Copyright (C) 2017 Claudio Maggioni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

import { Component } from '@angular/core';
import { ToastController, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ArgoUser } from '../home/home';

@Component({
    selector: 'page-config',
    templateUrl: 'config.html',
})
export class ConfigPage {
    user: ArgoUser = { username: "", password: "", schoolCode: "" };

    constructor(public navCtrl: NavController, public navParams: NavParams,
        private storage: Storage, private toastCtrl: ToastController) {
        this.storage.get('user').then(u => {
            if (u != null) this.user = u;
            console.log(this.user);
        }).catch(_ => { });
    }

    save(): void {
        this.user.schoolCode = this.user.schoolCode.toUpperCase();

        this.storage.set('user', this.user).then(_ => {
            this.toastCtrl.create({
                message: "Dati salvati.",
                duration: 3000
            }).present();
        }).catch(_ => {
            this.toastCtrl.create({
                message: "Errore nel salvataggio dei dati.",
                duration: 3000
            }).present();
        })
    }
}
