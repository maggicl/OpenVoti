/**
 * OpenVoti - Unofficial client for the Argo Scuolanext eDiary
 * Copyright (C) 2017 Claudio Maggioni
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 **/

import { Component, Input } from '@angular/core';
import { ArgoSubject, ArgoMark } from '../../pages/home/home';
import { trigger, state, style, animate, transition } from '@angular/animations';
import * as moment from 'moment';

@Component({
    selector: 'subject',
    templateUrl: 'subject.html',
    animations: [
        trigger('expandedChanged', [
            state('yes', style({ height: "*" })),
            state('no', style({ height: 0 })),
            transition('* => *', animate('100ms'))
        ])
    ]
})
export class SubjectComponent {
    @Input() subject: ArgoSubject;

    expanded: 'yes' | 'no' = 'no';
    loaded: boolean = false;
    marks: { [mType: string]: ArgoMark[] } = {};
    avg: { [mType: string]: number | '--' } = {};

    toggleExpanded(): void {
        if (this.expanded == 'yes') this.expanded = 'no'
        else this.expanded = 'yes'
    }

    constructor() { }

    ngAfterViewChecked() {
        for (let mType of ['scritto', 'orale', 'pratico']) {
            let ind = mType[0];

            this.marks[ind] = this.subject.marks.filter(m => {
                return m.type.toLowerCase() == mType
            });

            if (this.marks[ind].length > 0) {
                this.avg[ind] = 0;
                this.marks[ind].forEach((value: ArgoMark, _, __) => {
                    this.avg[ind] = <number>this.avg[ind] + value.value;
                });
                this.avg[ind] = <number>this.avg[ind] / this.marks[ind].length;
            } else {
                this.avg[ind] = '--';
            }
        }

        console.debug("SubjectComponent is: " + JSON.stringify(this));
        this.loaded = true;
    }

    getAvg(mType: string): string {
        if (this.loaded) {
            let tmp = this.avg[mType[0].toLowerCase()];
            if (tmp == '--') return '--';
            else return this.format(tmp);
        } else return '';
    }

    format(n: number): string {
        return n.toFixed(2);
    }

    formatDate(d: Date): string {
        return moment(d).format('DD/MM/YYYY')
    }
}
